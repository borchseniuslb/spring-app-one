package org.my.multi.module.one.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Hello from App one (1) on application start.
 */
@Component
public class StartupBeanOne {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void init() {
        logger.info("App one (1) reporting in");
    }
}